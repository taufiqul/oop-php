<?php

require("animal.php");
require("frog.php");
require("ape.php");

$sheep = new Animal("shaun");
echo "Nama hewan : $sheep->name <br>";
echo "Legs : $sheep->legs <br>";
echo "Coold blooderd : $sheep->cold_blooded <br>";
echo "<br>";

$kodok = new Frog("buduk");
echo "Nama hewan : $kodok->name <br>";
echo "Legs : $kodok->legs <br>";
echo "Coold blooderd : $kodok->cold_blooded <br>";
echo $kodok->jump();
echo "<br><br>";

$sungokong = new Ape("kera sakti");
echo "Nama hewan : $sungokong->name <br>";
echo "Legs : $sungokong->legs <br>";
echo "Coold blooderd : $sungokong->cold_blooded <br>";
echo $sungokong->yell();


?>